# Taller App

### Pre-requisitos:
    - Python > 3.3
    - Docker (https://docs.docker.com/engine/install/)
    - Docker Compose (https://docs.docker.com/compose/install/)

### Instalar venv
    Dentro de la carpeta del proyecto correr el siguiente comando **python3 -m venv [nombre_ambiente]** en caso de usar una distribucion de ubuntu, si ese no es el caso entonces correr **python -m venv [nombre_ambiente]**

    Ya una ves creado se puede activar con el siguiente comando **source [nombre_ambiente]/bin/activate**, para desactivar solo es necesario el siguiente comando **deactivate**

### Instalar proyecto
    Correr el comando **pip install -r requirements.txt**

### Instalar Mysql
    Si se corre el servidor saldra el siguiente error **(1049, "Unknown database 'taller_db'")**, eso significa que hace falta crear la base de datos, para eso se ejecuta el comando **docker ps** para mostrar una lista de los contenedores que se estan ejecutando, de ahi solo queda correr el comando **docker exec -it  taller_db_1 bash**, una ves dentro del contenedor se pondra el siguiente comando **mysql -uroot -p** de ahi pedira la contrasena que es pass1234. una ves ya dentro simplemente se creara la base de datos taller_db

### Correr server
    Una ves ya todo realizado solo queda correr el server con **python manage.py runserver**